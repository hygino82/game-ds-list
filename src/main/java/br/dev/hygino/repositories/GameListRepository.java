package br.dev.hygino.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.dev.hygino.entities.GameList;

public interface GameListRepository extends JpaRepository<GameList, Long> {

}
