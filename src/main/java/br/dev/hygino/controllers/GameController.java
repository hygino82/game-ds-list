package br.dev.hygino.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.dev.hygino.dto.GameInsertDTO;
import br.dev.hygino.dto.GameMinDTO;
import br.dev.hygino.services.GameService;
import jakarta.validation.Valid;

@RestController
@RequestMapping(value = "api/v1/game")
public class GameController {

    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping
    public ResponseEntity<List<GameMinDTO>> findAll() {
        return ResponseEntity.ok(gameService.findAll());
    }

    @PostMapping
    public ResponseEntity<GameMinDTO> insert(@RequestBody @Valid GameInsertDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(gameService.insert(dto));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") long id) {
        return gameService.findById(id);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody @Valid GameInsertDTO dto) {
        return gameService.update(id, dto);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") long id) {
        gameService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
