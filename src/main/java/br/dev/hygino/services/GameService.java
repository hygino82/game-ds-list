package br.dev.hygino.services;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.dev.hygino.dto.GameDTO;
import br.dev.hygino.dto.GameInsertDTO;
import br.dev.hygino.dto.GameMinDTO;
import br.dev.hygino.entities.Game;
import br.dev.hygino.projections.GameMinProjection;
import br.dev.hygino.repositories.GameRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@Service
public class GameService {
	private final GameRepository gameRepository;

	public GameService(GameRepository gameRepository) {
		this.gameRepository = gameRepository;
	}

	@Transactional(readOnly = true)
	public List<GameMinDTO> findAll() {
		List<Game> list = gameRepository.findAll();
		return list.stream().map(x -> new GameMinDTO(x)).toList();
	}

	@Transactional
	public GameMinDTO insert(@Valid GameInsertDTO dto) {
		var entity = new Game(dto);
		entity = gameRepository.save(entity);
		return new GameMinDTO(entity);
	}

	@Transactional(readOnly = true)
	public ResponseEntity<?> findById(long id) {
		Optional<Game> gameOptional = this.gameRepository.findById(id);
		if (gameOptional.isPresent()) {
			return ResponseEntity.ok(new GameDTO(gameOptional.get()));
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Game with Id: " + id + " not found");
		}
	}

	@Transactional
	public ResponseEntity<?> update(long id, @Valid GameInsertDTO dto) {
		try {
			Game entity = gameRepository.getReferenceById(id);
			entity = gameRepository.save(dtoToEntity(entity, dto));
			return ResponseEntity.ok(new GameMinDTO(entity));
		} catch (EntityNotFoundException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("You can't update, the game with Id " + id + " does not exists!");
		}
	}

	private Game dtoToEntity(Game entity, GameInsertDTO dto) {
		entity.setGenre(dto.genre());
		entity.setTitle(dto.title());
		entity.setImgUrl(dto.imgUrl());
		entity.setGenre(dto.genre());
		entity.setPlatforms(dto.platforms());
		entity.setYear(dto.year());
		entity.setLongDescription(dto.longDescription());
		entity.setScore(dto.score());
		entity.setShortDescription(dto.shortDescription());
		return entity;
	}

	public void delete(long id) {
		gameRepository.deleteById(id);
	}

	@Transactional(readOnly = true)
	public List<GameMinDTO> findByGameList(Long listId) {
		List<GameMinProjection> games = gameRepository.searchByList(listId);
		return games.stream().map(x -> new GameMinDTO(x)).toList();
	}
}
