package br.dev.hygino.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.dev.hygino.dto.GameListDTO;
import br.dev.hygino.entities.GameList;
import br.dev.hygino.repositories.GameListRepository;

@Service
public class GameListService {

	private final GameListRepository gameListRepository;

	public GameListService(GameListRepository gameListRepository) {
		this.gameListRepository = gameListRepository;
	}

	@Transactional(readOnly = true)
	public List<GameListDTO> findAll() {
		List<GameList> result = gameListRepository.findAll();
		return result.stream().map(GameListDTO::new).toList();
	}

	@Transactional(readOnly = true)
	public GameListDTO findById(Long id) {
		GameList entity = gameListRepository.findById(id).get();
		return new GameListDTO(entity);
	}
}