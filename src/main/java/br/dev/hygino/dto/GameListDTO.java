package br.dev.hygino.dto;

import br.dev.hygino.entities.GameList;

public record GameListDTO(
        Long id,
        String name) {
    public GameListDTO(GameList entity) {
        this(entity.getId(), entity.getName());
    }
}
