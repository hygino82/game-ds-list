package br.dev.hygino.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record GameInsertDTO(
                @NotBlank String title,
                @NotNull Integer year,
                String platforms,
                String genre,
                Double score,
                String imgUrl,
                String shortDescription,
                String longDescription) {
}
