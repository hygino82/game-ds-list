package br.dev.hygino.dto;

import br.dev.hygino.entities.Game;

public record GameDTO(
        Long id,
        String title,
        Integer year,
        String platforms,
        String genre,
        Double score,
        String imgUrl,
        String shortDescription,
        String longDescription) {
    public GameDTO(Game entity) {
        this(entity.getId(), entity.getTitle(), entity.getYear(), entity.getPlatforms(), entity.getGenre(),
                entity.getScore(), entity.getImgUrl(), entity.getShortDescription(), entity.getLongDescription());
    }
}
